resource "azurerm_network_interface" "main" {
  name                = "${var.business_divsion}-${var.environment}-${var.nic_name}" 
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = var.nic_ip_name
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
	  public_ip_address_id          = var.public_ip_address_id
  }
}