# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

# Azure Vnet name
variable "vnet_name" {
  description = "Vnet name"
  type = string
  default = "default-network"
}

# Azure Vnet name
variable "subnet_name" {
  description = "Vnet name"
  type = string
  default = "default-subnet"
}

# Azure subnet Address space 
variable "sn_address_space" {
  description = "subnet Address space"
  type = string
  default = "10.0.1.0/24"
}