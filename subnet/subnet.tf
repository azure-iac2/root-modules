# Create subnet
resource "azurerm_subnet" "subnet" {
  name                 = "${var.business_divsion}-${var.environment}-${var.subnet_name}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
  address_prefixes     = ["${var.sn_address_space}"]
}