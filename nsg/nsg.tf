## creation of Azure Network security group

resource "azurerm_network_security_group" "nsg" {
  name                = "${var.business_divsion}-${var.environment}-${var.nsg_name}" 
  location            = var.location
  resource_group_name = var.resource_group_name

  security_rule {
    name                       = var.nsg-security-rule-name
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

