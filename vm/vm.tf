resource "azurerm_virtual_machine" "main" {
  name                  = "${var.business_divsion}-${var.environment}-${var.vm_name}" 
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [var.nic_id]
  vm_size               = var.vm_size
  delete_os_disk_on_termination = "true"
	
  # Uncomment this line to delete the OS disk automatically when deleting the VM
  # delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true
  
  plan {
    name      = "tomcat-ubuntu"
    publisher = "cloud-infrastructure-services"
    product   =  "tomcat-ubuntu"
  }

  storage_image_reference {
    publisher = "cloud-infrastructure-services"
    offer     = "tomcat-ubuntu"
    sku       = "tomcat-ubuntu"
    version   = "latest"
  }
  storage_os_disk {
    name              = "osdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = var.computer_name
    admin_username = var.admin_username
    admin_password = var.admin_password
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags = {
    environment = var.environment
  }
}