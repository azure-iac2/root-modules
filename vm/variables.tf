# Generic Input Variables

# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = "default"
}

variable "vm_name" {
  description = "virtual machine name"
  type = string
  default = "default_vm_name"
}

variable "nic_id" {
  description = "NIC ID"
  type = string
  default = "default_nic_id"
}

variable "vm_size" {
  description = "VM Size"
  type = string
  default = "default_vm_size"
}

variable "computer_name" {
  description = "OS Computer name"
  type = string
  default = "default_computer_name"
}

variable "admin_username" {
  description = "OS computer admin name"
  type = string
  default = "default_admin_name"
}

variable "admin_password" {
  description = "OS Computer admin password"
  type = string
  default = "default_admin_password"
}
