## Azure Vnet resource creation
data "azurerm_resource_group" "rg"{
  name = var.resource_group_name
}

resource "azurerm_virtual_network" "vnet" {
  name               = "${var.business_divsion}-${var.environment}-${var.vnet_name}"
  address_space       = ["${var.address_space}"]
  location            = var.location
  resource_group_name = var.resource_group_name
}
