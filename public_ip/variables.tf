# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = "default"
}

variable "public_ip_name" {
  description = "public ip name"
  type = string
  default = "default"
}

variable "allocation_method" {
  description = "public ip allocation method"
  type = string
  default = "default"
}
