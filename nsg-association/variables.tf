variable "network_interface_id" {
  description = "network interface id"
  type = string
  default = "default-id"
}

variable "network_security_group_id" {
  description = "network security group id"
  type = string
  default = "default-id"
}
