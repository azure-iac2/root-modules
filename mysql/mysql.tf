resource "azurerm_mysql_server" "mysql_server" {
  name                = "${var.business_divsion}-${var.environment}-${var.my_server_name}"
  location            = var.location
  resource_group_name = var.resource_group_name

  administrator_login          = var.my_server_admin_login
  administrator_login_password = var.my_server_admin_password

  sku_name   = "GP_Gen5_2"
  storage_mb = 5120
  version    = "5.7"

  auto_grow_enabled                 = true
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = true
  infrastructure_encryption_enabled = true
  public_network_access_enabled     = false
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_mysql_database" "mysql_db" {
  name                = "${var.business_divsion}-${var.environment}-${var.my_db_name}"
  resource_group_name = var.resource_group_name
  server_name         = "${var.business_divsion}-${var.environment}-${var.my_server_name}"
  charset             = "utf8"
  collation           = "utf8_unicode_ci"

  depends_on = [azurerm_mysql_server.mysql_server]
}

resource "azurerm_mysql_firewall_rule" "mysql_firewall" {
  name                = "${var.business_divsion}-${var.environment}-${var.my_server_firewall_rule_name}"
  resource_group_name = var.resource_group_name
  server_name         = "${var.business_divsion}-${var.environment}-${var.my_server_name}"
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"

  depends_on = [azurerm_mysql_server.mysql_server]
}