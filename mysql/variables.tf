# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = "default"
}

variable "my_server_name" {
  description = "my sql server name"
  type = string
  default = "default"
}

variable "my_server_admin_login" {
  description = "my sql admin login name"
  type = string
  default = "default"
}

variable "my_server_admin_password" {
  description = "my sql server admin login password"
  type = string
  default = "default"
}

variable "my_db_name" {
  description = "my sql db name"
  type = string
  default = "default"
}

variable "my_server_firewall_rule_name" {
  description = "my sql server admin login password"
  type = string
  default = "default"
}
